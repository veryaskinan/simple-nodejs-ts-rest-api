FROM node:latest

WORKDIR /app

COPY ./ /app

EXPOSE 1029

ENTRYPOINT ["sh", "start.sh"]
