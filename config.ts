import dotenv from 'dotenv';
import path from 'path';

dotenv.config({ path: path.resolve(__dirname, '.env') });

const env = process.env;

interface Config {
    db: {
        host: string
        port: number
        user: string
        password: string
        name: string
    }
    auth: {
        tokenLifetime: number
    }
}

export const config: Config = {
    db: {
        host: env.db_host || 'localhost',
        port: Number(env.db_port) || 5432,
        user: env.db_user || 'root',
        password: env.db_password || 'root',
        name: env.db_name || 'postgres'
    },
    auth: {
        tokenLifetime: Number(env.auth_token_lifetime) || 10
    }
}