import { TRouterSchema } from '../infrastructure/server/TRouterSchema'
import { AuthController } from './auth'
import { UsersController } from './users'
import { PingController } from './ping'

export const routerSchema: TRouterSchema = {
    '/signin': {
        'post': {
            appMethod: AuthController.signIn,
            tokenRequired: false
        }
    },
    '/signup': {
        'post': {
            appMethod: AuthController.signUp,
            tokenRequired: false
        }
    },
    '/user': {
        'get': UsersController.getCurrentUser
    },
    '/ping': {
        'get': PingController.ping
    },
    '/logout': {
        'get': AuthController.logOut
    }
}