import express from 'express';
import { AuthService } from '../services';
import { TokenService } from '../../tokens';

export class AuthController {

    public static async signUp(req: express.Request, res: express.Response): Promise<void> {
        const token = await AuthService.signUp(req.body.id, req.body.password)
        res.status(200).send({
            token: token
        });
    }

    public static async signIn(req: express.Request, res: express.Response): Promise<void> {
        const token = await AuthService.signIn(req.body.id, req.body.password)
        res.status(200).send({
            token: token
        });
    }

    public static async logOut(req: express.Request, res: express.Response): Promise<void> {
        const token = TokenService.getTokenFromRequest(req)
        await AuthService.logOut(req.body.all, token)
        res.status(204).send();
    }

}