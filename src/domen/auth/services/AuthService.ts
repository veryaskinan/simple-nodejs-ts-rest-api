import { UsersService } from '../../users'
import { TokenService } from "../../tokens";
import { RegistrationType } from '../enums'
import { PasswordService } from "./PasswordService";

export class AuthService {

    public static async signUp(phoneOrEmail: string, password:string): Promise<string> {
        const registrationType = AuthService.identifyRegistrationType(phoneOrEmail)

        const userExists = await UsersService.userExists(phoneOrEmail)

        if (userExists) {
            throw new Error('Token exists')
        }

        const idField = AuthService.identifyIdField(registrationType)
        const passwordHash = PasswordService.hash(password)

        await UsersService.createUser({
            [idField]: phoneOrEmail,
            password_hash: passwordHash,
            registration_type: registrationType,
        })

        return await AuthService.signIn(phoneOrEmail, password)
    }

    public static async signIn(id: string, password:string): Promise<string> {
        const user = await UsersService.getUserByPhoneOrEmail(id)
        PasswordService.validate(user.password_hash, password)
        return await TokenService.create(user.id)
    }

    public static async logOut(all: boolean, token: string): Promise<void> {
        switch(all) {
            case true:
                const dbToken = await TokenService.getByToken(token)
                await TokenService.deleteTokensByUserId(dbToken.user_id)
                break
            case false:
                await TokenService.deleteToken(token)
                break
        }
    }

    private static identifyRegistrationType(id: string): RegistrationType {
        if (AuthService.hasOnlyDigits(id)) {
            return RegistrationType.phone
        } else if (AuthService.isEmail(id)) {
            return RegistrationType.email
        }
        throw new Error('Incorrect id')
    }

    private static identifyIdField(registrationType: RegistrationType): 'phone' | 'email' {
        switch (registrationType) {
            case RegistrationType.phone:
                return 'phone'
            case RegistrationType.email:
                return 'email'
        }
    }

    private static hasOnlyDigits(string: string): boolean {
        return string === String(+string)
    }

    private static isEmail(string: string): boolean {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        return re.test(String(string).toLowerCase())
    }
}