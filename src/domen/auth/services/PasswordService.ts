import bcrypt from "bcrypt";

export class PasswordService {

    public static hash(passsword: string): String {
        const salt = bcrypt.genSaltSync(10)
        const hash = bcrypt.hashSync(passsword, salt)
        return hash
    }

    public static validate(passsword_hash:string, passsword: string): String {
        const salt = bcrypt.genSaltSync(10)
        const hash = bcrypt.hashSync(passsword, salt)
        return hash
    }

}