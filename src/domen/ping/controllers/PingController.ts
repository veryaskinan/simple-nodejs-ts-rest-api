import express from 'express';
import { PingService } from '../services';

export class PingController {

    public static async ping(req: express.Request, res: express.Response): Promise<void> {
        const ping = await PingService.ping()
        res.status(200).send({ ping: ping });
    }

}