import axios from 'axios';

export class PingService {

    public static async ping(): Promise<number> {
        const start = Date.now()
        await axios.get('https://google.com')
        return Date.now() - start
    }

}