import {Table, Column, Model, DataType, ForeignKey, HasOne, PrimaryKey, AutoIncrement} from 'sequelize-typescript'

@Table({
    tableName: 'tokens',
    timestamps: false,
})
export class Token extends Model {

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    id!: number

    @Column(DataType.STRING)
    token!: number

    @Column(DataType.INTEGER)
    user_id!: number

    @Column(DataType.DATE)
    expire_time!: Date

}