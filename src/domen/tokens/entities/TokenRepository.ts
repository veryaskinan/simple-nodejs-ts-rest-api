import moment from 'moment'
import { Op } from 'sequelize'
import { connection } from '../../../infrastructure/db/connection'
import { Token } from './Token'

connection.addModels([Token])

export interface TokenUpdatedFields {
    expiredTime: moment.Moment
}

export class TokenRepository {

    private static repository = connection.getRepository(Token)
    private static tokenNotFoundErrorMessage = 'Token not found'

    public static async getByToken(token: string): Promise<Token> {
        const dbToken = await TokenRepository.repository.findOne({
            where: {
                token: token
            }
        })
        if (dbToken === null) {
            throw new Error(TokenRepository.tokenNotFoundErrorMessage)
        }
        return dbToken
    }

    public static async update(dbToken: Token, tokenUpdatedFields: TokenUpdatedFields): Promise<void> {
        await TokenRepository.repository.sequelize?.query(`
            update tokens set expire_time = :expireTime where id = :tokenId;
        `, {
            replacements: {
                expireTime: tokenUpdatedFields.expiredTime.format('YYYY-MM-DD HH:mm:ss'),
                tokenId: dbToken.id
            }
        })
    }

    public static async createToken(token: string, userId: number, expireTime: moment.Moment): Promise<Token> {
        const expireTimeString = expireTime.format('YYYY-MM-DD HH:mm:ss')
        return await TokenRepository.repository.create({
            token: token,
            user_id: userId,
            expire_time: expireTime.format('YYYY-MM-DD HH:mm:ss')
        })
    }

    public static async deleteTokensByUserId(userId: number): Promise<void> {
        await TokenRepository.repository.sequelize?.query(`
            delete from tokens where user_id = :userId;
        `, {
            replacements: {
                userId: userId
            }
        })
    }

    public static async deleteToken(token: string): Promise<void> {
        await TokenRepository.repository.sequelize?.query(`
            delete from tokens where token = :token;
        `, {
            replacements: {
                token: token
            }
        })
    }

}