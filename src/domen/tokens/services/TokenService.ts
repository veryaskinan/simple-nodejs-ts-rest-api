import moment from 'moment'
import express from 'express';
import randomstring from 'randomstring'
import { config } from '../../../../config'
import { TokenRepository, Token } from '../entities'

export class TokenService {

    public static async create(userId: number): Promise<string> {
        const token = TokenService.generate(userId)

        const expireTime = moment().add(config.auth.tokenLifetime, 'minutes')

        await TokenRepository.createToken(token, userId, expireTime)

        return token
    }

    public static async validate(token: string): Promise<void> {
        try {
            const dbToken = await TokenRepository.getByToken(token)
            const expireTime = moment(dbToken.expire_time).utc(true)
            const nowTime = moment().utc()
            if (nowTime.isAfter(expireTime)) {
                throw new Error()
            }
        } catch (error) {
            throw new Error('Incorrect token')
        }
    }

    public static async prolong(token: string): Promise<void> {
        const dbToken = await TokenRepository.getByToken(token)
        const newExpireTime = moment().add(config.auth.tokenLifetime, 'minutes')
        await TokenRepository.update(dbToken, {
            expiredTime: newExpireTime
        })
    }

    public static getTokenFromRequest(req: express.Request): string {
        if (!req.headers.authorization) {
            throw new Error('Authorization header not found')
        }
        return req.headers.authorization.substr(7)
    }

    public static async deleteTokensByUserId(userId: number): Promise<void> {
        await TokenRepository.deleteTokensByUserId(userId)
    }

    public static async deleteToken(token: string): Promise<void> {
        await TokenRepository.deleteToken(token)
    }

    public static async getByToken(token: string): Promise<Token> {
        return await TokenRepository.getByToken(token)
    }

    private static generate(userId: number): string {
        const encodedUserId = String(userId)
            .replace('0', 't')
            .replace('1', 'r')
            .replace('2', 'Q')
            .replace('3', 'z')
            .replace('4', 'i')
            .replace('5', 'J')
            .replace('6', 'w')
            .replace('7', 'P')
            .replace('8', 'T')
            .replace('9', 'l')
        const randomString = randomstring.generate(64)
        return encodedUserId + randomString
    }
}