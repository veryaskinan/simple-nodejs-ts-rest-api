import express from 'express'
import { TokenService } from '../../tokens'
import { UsersService } from '../services'

export class UsersController {

    public static async getCurrentUser(req: express.Request, res: express.Response): Promise<void> {
        const token = TokenService.getTokenFromRequest(req)
        const userDto = await UsersService.getUserByToken(token)
        res.status(200).send(userDto);
    }

}