import { RegistrationType } from "../../auth";
import { User } from "../entities";
import { IUserDto } from "./IUserDto";

export class UsersMapper {

    public static toDto(user: User): IUserDto {
        return {
            id: user.id,
            registrationType: RegistrationType[user.registration_type]
        }
    }

}