import { Table, Column, Model, DataType, AutoIncrement, PrimaryKey, HasMany } from 'sequelize-typescript'

@Table({
    tableName: 'users',
    timestamps: false,
})
export class User extends Model {

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    id!: number

    @Column(DataType.STRING(64))
    email!: string | null

    @Column(DataType.STRING(32))
    phone!: string | null

    @Column(DataType.STRING(1024))
    password_hash!: string

    @Column(DataType.INTEGER)
    registration_type!: number

}