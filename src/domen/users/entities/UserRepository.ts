import {WhereOptions} from "sequelize";

const { Op } = require("sequelize");
import { connection } from '../../../infrastructure/db/connection'
import { User } from './User'

connection.addModels([User])

export class UserRepository {

    private static repository = connection.getRepository(User)
    private static userNotFoundErrorMessage = 'User not found'

    public static async getUserById(id: number): Promise<User> {
        return await UserRepository.getByCondition({
            id: id,
        })
    }

    public static async getByPhoneOrEmail(phoneOrEmail: string): Promise<User> {
        return await UserRepository.getByCondition({
            [Op.or]: [
                { phone: phoneOrEmail },
                { email: phoneOrEmail },
            ],
        })
    }

    public static async userExists(phoneOrEmail: string): Promise<boolean> {
        try {
            const user = await UserRepository.getByPhoneOrEmail(phoneOrEmail)
        } catch (error) {
            if (error.message === UserRepository.userNotFoundErrorMessage) {
                return false
            }
            throw error
        }
        return true
    }

    public static async createUser(userFields: Record<string, any>): Promise<User> {
        return await UserRepository.repository.create(userFields)
    }

    private static async getByCondition(condition: WhereOptions): Promise<User> {
        const user = await UserRepository.repository.findOne({
            where: condition
        })
        if (user === null) {
            throw new Error(UserRepository.userNotFoundErrorMessage)
        }
        return user
    }
}