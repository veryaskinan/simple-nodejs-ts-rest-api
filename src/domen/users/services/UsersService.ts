import { TokenService } from '../../tokens'
import { UserRepository, User } from '../entities'
import { UsersMapper, IUserDto } from '../dto'

export class UsersService {

    public static async getUserByPhoneOrEmail(phoneOrEmail: string): Promise<User> {
        return await UserRepository.getByPhoneOrEmail(phoneOrEmail)
    }

    public static async getUserByToken(token: string): Promise<IUserDto> {
        const dbToken = await TokenService.getByToken(token)
        return await UsersService.getUserById(dbToken.user_id)
    }

    public static async getUserById(id: number): Promise<IUserDto> {
        const user = await UserRepository.getUserById(id)
        return UsersMapper.toDto(user)
    }

    public static async userExists(phoneOrEmail: string): Promise<Boolean> {
        return await UserRepository.userExists(phoneOrEmail)
    }

    public static async createUser(fields:  Record<string, any>): Promise<User> {
        return await UserRepository.createUser(fields)
    }
}