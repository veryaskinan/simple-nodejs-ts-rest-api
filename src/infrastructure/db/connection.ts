import { Sequelize } from 'sequelize-typescript'
import { config } from '../../../config'

export const connection = new Sequelize({
    repositoryMode: true,
    dialect: 'postgres',
    host: config.db.host,
    port: config.db.port,
    username: config.db.user,
    password: config.db.password,
    database: config.db.name,
    storage: ':memory:',
})