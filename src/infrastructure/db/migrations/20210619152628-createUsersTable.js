'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(`
      begin;
        create sequence if not exists user_id;
        create table if not exists users (
          id integer default nextval('user_id'),
          email varchar(64),
          phone varchar(32),
          password_hash varchar(1024),
          registration_type smallint,
          primary key (id)
        );
      commit;
    `)
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('users');
  }
};
