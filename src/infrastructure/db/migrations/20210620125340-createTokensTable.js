'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(`
      begin;
        create sequence if not exists token_id;
        create table if not exists tokens (
          id integer default nextval('token_id'),
          user_id integer,
          token varchar(128),
          expire_time timestamp,
          primary key (id)
        );
      commit;
    `)
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tokens');
  }
};
