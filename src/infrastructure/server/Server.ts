import express from 'express'
import { router } from './middlewares/Router'
import { errorHandler } from './middlewares/ErrorHandler'
import { routerSchema } from '../../domen'

const app: express.Application = express()

app.use(express.json())
app.use(router(routerSchema))
app.use(errorHandler)

app.listen(1029, function () {
    console.log('App is listening 1029 port');
});