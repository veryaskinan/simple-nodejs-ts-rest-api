export type TRouteOptions = {
    appMethod: Function
    tokenRequired: boolean
}
export type TRoute = Record<string, Function | TRouteOptions>
export type TRouterSchema = Record<string, TRoute>