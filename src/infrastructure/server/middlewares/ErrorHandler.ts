import express from 'express'

export function errorHandler(error: any, req: express.Request, res:express.Response, next: express.NextFunction) {
    if (error) {
        res.status(500).send({
            message: error.message,
            stack: error.stack,
        });
    }
    next()
}