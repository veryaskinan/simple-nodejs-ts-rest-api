import express from 'express'
import { TokenService } from "../../../domen/tokens"
import { TRouterSchema } from "../TRouterSchema"

export function router(routerSchema: TRouterSchema) {
    return async (req: express.Request, res:express.Response, next: express.NextFunction) => {
        const route = getRoute(req, routerSchema)
        try {
            if (route.tokenRequired) {
                const token = TokenService.getTokenFromRequest(req)
                await TokenService.validate(token)
                await TokenService.prolong(token)
            }
            await route.appMethod(req, res)
        } catch (error: any) {
            next(error)
        }
        next()
    }
}

interface IRoute {
    httpMethod: string,
    appMethod: Function,
    tokenRequired: boolean,
}

function getRoute(req: express.Request, routerSchema: TRouterSchema): IRoute {
    const route: IRoute = {
        httpMethod: req.method.toLowerCase(),
        appMethod: () => {},
        tokenRequired: true,
    }
    const routeOptions = routerSchema[req.path][route.httpMethod]
    if (typeof routeOptions === 'function') {
        route.appMethod = routeOptions
    } else {
        route.appMethod = routeOptions.appMethod
        route.tokenRequired = routeOptions.tokenRequired
    }
    return route
}